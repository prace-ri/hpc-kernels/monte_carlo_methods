## Compilation instructions

The pre-requisites are:

* CMake 2.8.10 or higher. CMake >= 3.0.0 if you want to build everything.
* A C++ compiler
* For advanced examples, C++11/14, OpenMP support, MPI, ISPC, CUDA, OpenCL ...

In order to build the code, follow the typical CMake steps for an
out-of-source build:

```bash
mkdir build
cd build
cmake ..
make
make install # optional
```
